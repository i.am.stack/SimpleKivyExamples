# MONSTER thanks goes to tshirtman, Fuyou, LogicalDash[m], and atis in the kivy
# IRC group for helping me learn. Tshirtman wrote several iterations of this
# code in various stages for me to grasp concepts. His code can be found here:
# https://gist.github.com/tshirtman/da8d1b548f60a41fa4076d6e1810320e

# This App generates buttons based on the list SomeList below. Add items to it.
# Move the screen size around. Play with it. See how it generates the screen!

# This is the main import for Kivy.
# https://kivy.org/docs/api-kivy.app.html
from kivy.app import App

# Builder lets me pass the kv code in as a string instead of a separate kv file
# https://kivy.org/docs/api-kivy.lang.builder.html
from kivy.lang import Builder

# ScreenManager helps me manage multiple screens. In this example, a home
# screen and a secondary templated screen. The FadeTransition is just the type
# of transition between the two screens.
# https://kivy.org/docs/api-kivy.uix.screenmanager.html
from kivy.uix.screenmanager import ScreenManager, Screen, FadeTransition

# The Factory object helps me programatically register widgets to add to Screen.
# https://kivy.org/docs/api-kivy.factory.html
from kivy.factory import Factory

# ObjectProperty helps me link the code to a specific kv entry.
#https://kivy.org/docs/api-kivy.properties.html#kivy.properties.ObjectProperty
from kivy.properties import ObjectProperty

# Button class gives me a button to manipulate on the screen.
# https://kivy.org/docs/api-kivy.uix.button.html
from kivy.uix.button import Button

# This is just an example of a list coming from "outside" the program.
# Modify this list to add/remove buttons.
SomeList = ["First", "Second", "Third"]

# The main class for the display. Not asking anything of it except via kv/code.
# This is what handles all of the management of the many screens for us.
class ScreenManagement(ScreenManager):
    pass

# This class is the first screen we use. Tying the box object to the kv code
# so that we can use it later.
class MainScreen(Screen):
    box = ObjectProperty()

# This is where we are defining our kv code.
kv = Builder.load_string('''
# Have to import the FadeTransition in the kv for it to work.
#: import FadeTransition kivy.uix.screenmanager.FadeTransition

# The main declerations for the kv. All screens have to be declared here
# unless they are a template. Also defines how screens interact.
ScreenManagement:
    transition: FadeTransition()
    MainScreen:
        # Assigning an ID so that we can reference this screen from code.
        id: mainscreen

# This is our main screen that we anchor against.
<MainScreen>:
    # The name of this screen
    name: "main"
    # This binds our property to an id. Should give more flexibility in code.
    # This is the 'glue' between our code and the boxlayout below.
    box: box
    # Defines the layout on the main screen
    BoxLayout:
        # This id links with the above property so we can reference it.
        id: box
        # Setting a orientation for how the boxes stack on the screen.
        orientation: 'vertical'
 
# The template that we use to generate new screens programatically.
<ScreenTemplate@Screen>:
    # No name assigned. Over ride on call.
    name: ""
    # This is the text in the center of the screen.
    Label:
        # a default size for the text.
        text_size: self.size
        # text goes in the center
        halign: 'center'
        # text goes in the middle
        valign: 'middle'
        # Text is what is passed to it.
        text: root.name
    # This is our button to return to the MainScreen
    Button:
        # Make it a different color because we can.
        color: 0,1,0,1
        # Set the size
        font_size: 25
        # Setting how big of a button it should be
        size_hint: 0.3,0.2
        # Set the text
        text: "Home"
        # Telling it to go back to the MainScreen
        on_release: app.root.current = "main"
        # Place it in the top right corner
        pos_hint: {"right":1, "top":1}
''')

# This class is what generates the buttons on the main screen.
class ButtonApp(App):
    # The work we need to do is this function.
    def build(self):
        # pbox is my variable to programatically add boxes to. It is instantiated
        # with the kv code (presentation), specifically the id box from mainscreen.
        pbox = kv.ids.mainscreen.box
        # Looping through the contents of our list.
        for item in SomeList:
            # Creating a new screen based on the template with the name of our item.
            screen = Factory.ScreenTemplate(name=item)
            # Add that to our ScreenManager
            kv.add_widget(screen)
            # Creating a Button. Python lambda's are like mini functions. x is like
            # the parameter we pass to the function. Everything else is the work done
            # in the function. In this case we are matching the button item name to
            # the generated template screen that we defined a few lines above.
            btn = Button(text=item, on_press=lambda x: kv.setter('current')(x, x.text))
            # Now we add that button to the pbox as a widget.
            pbox.add_widget(btn)
        # Once we have all of our buttons, we can run the display.
        return kv

# This is how the program starts. Basic Python start up here.
if __name__ == "__main__":
    # Telling it to run the ButtonApp class.
    ButtonApp().run()
