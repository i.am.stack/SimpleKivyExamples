# This is the most simple of Kivy apps Starting here.
# The point is to be a simple intro into Kivy.
# Trying to be explicit.

# Import Kivy App which is the base class for Kivy Applications.
# https://kivy.org/docs/api-kivy.app.html
from kivy.app import App

# This is your main class. It should end with 'App' as Kivy uses it.
class StartApp(App):
    # We are not doing anything, so pass.
    pass

# This is how Python realizes to start the application
if __name__ == "__main__":
    # We are only going to run our class.
    StartApp().run()
