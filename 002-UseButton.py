# We are going to take the button and define it in the Kivy language.

# Import Kivy App which is the base class for Kivy Applications.
# https://kivy.org/docs/api-kivy.app.html
from kivy.app import App

# Import Button
# https://kivy.org/docs/api-kivy.uix.button.html
from kivy.uix.button import Button

# Import Builder class.
# https://kivy.org/docs/api-kivy.lang.builder.html
from kivy.lang import Builder

# We need a new class to reference our Kivy.
class KvButton(Button):
    pass

# The builder is going to load our Kivy language string. This will make it
# easier to define Kivy layouts.
# Having it all in one file helps when you need to get help from Kivy groups:
# https://github.com/kivy/kivy/wiki/Community-Guidelines
kv = Builder.load_string('''
# Defining our root.
KvButton:

# Configuring our Screen.
<KvButton>:
    # Assigning a Button
    Button:
        # Defining Button text.
        text: "Hello World!"
        # Putting in center of screen.
        size: root.size
''')

# This is your main class.
class UseButtonApp(App):
    # Using the build method to initialize the application.
    # https://kivy.org/docs/api-kivy.app.html#kivy.app.App.build
    #
    # Self is a Python way of referring to the object created in this class.
    def build(self):
        # We have to return kv for the application to use it.
        return kv

# Tell Python to start the app.
if __name__ == "__main__":
    # We are only going to run our class.
    UseButtonApp().run()
