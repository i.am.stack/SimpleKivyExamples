# Add something more complex; like painting!
# This is a modification to the tutorial done by inclement
# called Kivy Crash Course found here:
# https://github.com/inclement/kivycrashcourse

# Import Kivy App which is the base class for Kivy Applications.
# https://kivy.org/docs/api-kivy.app.html
from kivy.app import App

# Import Builder class.
# https://kivy.org/docs/api-kivy.lang.builder.html
from kivy.lang import Builder

# Because we will be doing multiple screens, we need Screen and
# a manager to help arrange it.
# https://kivy.org/docs/api-kivy.uix.screenmanager.html
from kivy.uix.screenmanager import ScreenManager, Screen

# Import widget to attach Button to.
# https://kivy.org/docs/api-kivy.uix.widget.html
from kivy.uix.widget import Widget

# Need this for the drawing.
# https://kivy.org/docs/api-kivy.graphics.html#kivy.graphics.Line
from kivy.graphics import Line

# This is the drawing class.
class Painter(Widget):
    # On press of click, start a line until released
    def on_touch_down(self, touch):
        with self.canvas:
            touch.ud["line"] = Line(points=(touch.x, touch.y))

    # As along as the touch/click is moving, draw a line.
    def on_touch_move(self,touch):
        touch.ud["line"].points += [touch.x, touch.y]
        
class MainScreen(Screen):
    pass

class AnotherScreen(Screen):
    pass

class FirstScreen(Screen):
    pass

class SecondScreen(Screen):
    pass
  
class ScreenManagement(ScreenManager):
    pass

# The builder is going to load our Kivy language string. This will make it
# easier to define Kivy layouts.
# Having it all in one file helps when you need to get help from Kivy groups:
# https://github.com/kivy/kivy/wiki/Community-Guidelines
kv = Builder.load_string('''
# Most everything below is seen in earlier examples.
ScreenManagement:
    MainScreen:
    AnotherScreen:
    SecondScreen:

<MainScreen>:
    name: "main"
    FloatLayout:
        Button:
            on_release: app.root.current = "other"
            text: "Paint"
            font_size: 50
            size_hint: 0.3,0.2
            pos_hint: {"top":1}
        Button:
            on_release: app.root.current = "second"
            text: "Second"
            font_size: 50
            size_hint: 0.3,0.2
            pos_hint: {"bottom":1}

<FirstScreen>:
    name: "first"
    Button:
        color: 0,1,0,1
        font_size: 25
        size_hint: 0.3,0.2
        text: "Home"
        on_release: app.root.current = "main"
        pos_hint: {"right":1, "top":1}

<SecondScreen>:
    name: "second"
    Button:
        color: 0,1,0,1
        font_size: 25
        size_hint: 0.3,0.2
        text: "Back Home"
        on_release: app.root.current = "main"
        pos_hint: {"right":1, "top":1}

<AnotherScreen>:
    name: "other"
    FloatLayout:
        Painter
        Button:
            color: 0,1,0,1
            font_size: 25
            size_hint: 0.3,0.2
            text: "Back Home"
            on_release: app.root.current = "main"
            pos_hint: {"right":1, "top":1}
        Button:
            color: 0,1,0,1
            font_size: 25
            size_hint: 0.3,0.2
            text: "Clear"
            on_release: self.parent.children[2].canvas.clear()
            pos_hint: {"right":1, "bottom":1}
''')

class MainApp(App):
    # Using the build method to initialize the application.
    # https://kivy.org/docs/api-kivy.app.html#kivy.app.App.build
    #
    # Self is a Python way of referring to the object created in this class.
    def build(self):
        return kv

if __name__ == "__main__":
    MainApp().run()
