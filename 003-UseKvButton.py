# We are going to take the button and define it in the Kivy language,
# but in a separate file this time. It is important that the file be
# all lowercase and named after your main app without the App ending.
# In this case usekvbutton.kv is the proper file name.

# Import Kivy App which is the base class for Kivy Applications.
# https://kivy.org/docs/api-kivy.app.html
from kivy.app import App

# Import Button
# https://kivy.org/docs/api-kivy.uix.button.html
from kivy.uix.button import Button

# Notice we don't need the builder class anymore.

class KvButton(Button):
    pass

# This is your main class.
class UseKvButtonApp(App):
    # Using the build method to initialize the application.
    # https://kivy.org/docs/api-kivy.app.html#kivy.app.App.build
    #
    # Self is a Python way of referring to the object created in this class.
    def build(self):
        # Because we are using a Kivy file, we can just pass.
        pass

# Tell Python to start the app.
if __name__ == "__main__":
    # We are only going to run our class.
    UseKvButtonApp().run()
